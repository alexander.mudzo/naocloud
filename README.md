# NaoCloud

This project is part of my Bachelor thesis at KEMT TUKE. This SW is used for remote control of Nao robot via Web interface.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

1) clone from [here](https://github.com/AlexanderMudzo/NaoCloud.git)
2) resovle project dependencies using Maven from public repos
3) compile and run 

### Prerequisites
* JDK 1.8
* Docker Container Runtime

### Installing 
1) confirure data source in `application.properties`
2) run build target install to create .JAR file
3) pack JAR archive (located in target dir) /w dockerfile 
4) deploy to tergeted cloud platform or using docker command create image or deploy on local system
to create docker image please refer to [Docker - Create a base image](https://docs.docker.com/develop/develop-images/baseimages/) oficial guide.
  

## Built With

* [Maven](https://maven.apache.org/)
* [Docker](https://www.docker.com/)

## Authors

* **Alexander Mudzo** - *Initial work* - [r4d1x](https://github.com/AlexanderMudzo)

See also the list of [contributors](https://github.com/AlexanderMudzo/NaoCloud/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Special thanks to : **Sean Guo** for NaoRobot control script

